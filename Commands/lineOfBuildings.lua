function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Expand current buildqueue by buildings of given type build along line defined by node parameters. Can wait until buildqueue is empty or just instantly succeed once buildings are added.",
		parameterDefs = {
			{ 
				name = "buildingName",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = '"armsolar"',
			},
			{ 
				name = "startPosition",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "core.Position()",
			},
			{ 
				name = "endPosition", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "core.Position() + Vec3(100,0,100)",
			},
			{ 
				name = "spacing",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "0",
			},
		}
	}
end

-- constants
local FOOTPRINT_POINT_SIZE = 8

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringTestBuildOrder = Spring.TestBuildOrder

function Run(self, units, parameters)
	local pointUnit = units[1]
	
	if (not self.inProgress) then
		local buildingName = parameters.buildingName -- string
		local startPosition = parameters.startPosition -- Vec3
		local endPosition = parameters.endPosition -- Vec3
		local spacing = parameters.spacing -- number in footprint size
		
		-- Spring.Echo(buildingName, startPosition, endPosition, spacing)
		
		local buildingDef = UnitDefNames[buildingName]
		if (buildingName == nil or buildingDef == nil) then
			Logger.error("build.constructAlongLine", "Unknown building name: '", buildingName, "'")
			return FAILURE
		end
		local buildingDefID = buildingDef.id

		-- get size constants		
		local buildingSizeXelmos = UnitDefNames[buildingName].xsize * FOOTPRINT_POINT_SIZE
		local buildingSizeZelmos = UnitDefNames[buildingName].zsize * FOOTPRINT_POINT_SIZE
		local spacingInElmos = spacing * FOOTPRINT_POINT_SIZE
		
		-- prepare step vector
		local lineVector = endPosition - startPosition
		local lineLength = lineVector:Length()
		local lineAngle = lineVector:ToHeading()
		local stepX
		local stepZ
		local steps
		
		if     (lineAngle == 45) then
			stepX = buildingSizeXelmos + spacingInElmos
			stepZ = -(buildingSizeZelmos + spacingInElmos)
			steps = math.abs(lineVector.x / stepX)
		elseif (lineAngle > 45 and lineAngle < 135) then
			stepX = buildingSizeXelmos + spacingInElmos
			stepZ = math.floor(lineVector.z / (lineVector.x / (buildingSizeXelmos + spacingInElmos)))
			steps = math.abs(lineVector.x / stepX)
		elseif (lineAngle == 135) then
			stepX = buildingSizeXelmos + spacingInElmos
			stepZ = buildingSizeZelmos + spacingInElmos
			steps = math.abs(lineVector.x / stepX)
		elseif (lineAngle > 135 and lineAngle < 225) then
			stepX = math.floor(lineVector.x / (lineVector.z / (buildingSizeZelmos + spacingInElmos)))
			stepZ = (buildingSizeZelmos + spacingInElmos)
			steps = math.abs(lineVector.z / stepZ)
		elseif (lineAngle == 225) then
			stepX = -(buildingSizeXelmos + spacingInElmos)
			stepZ = buildingSizeZelmos + spacingInElmos
			steps = math.abs(lineVector.z / stepZ)
		elseif (lineAngle > 225 and lineAngle < 315) then
			stepX = -(buildingSizeXelmos + spacingInElmos)
			stepZ = math.floor(lineVector.z / (lineVector.x / (-(buildingSizeXelmos + spacingInElmos))))
			steps = math.abs(lineVector.x / stepX)
		elseif (lineAngle == 315) then
			stepX = -(buildingSizeXelmos + spacingInElmos)
			stepZ = -(buildingSizeZelmos + spacingInElmos)
			steps = math.abs(lineVector.x / stepX)
		else
			stepX = math.floor(lineVector.x / (lineVector.z / (-(buildingSizeZelmos + spacingInElmos))))
			stepZ = -(buildingSizeZelmos + spacingInElmos)
			steps = math.abs(lineVector.z / stepZ)
		end
		
		local steps = math.floor(steps) + 1
		local stepVector = Vec3(stepX, 0, stepZ)
		
		-- Spring.Echo(startPosition.x, startPosition.z, endPosition.x, endPosition.z, lineAngle, stepX, stepZ, steps)
		
		-- prepare valid positions
		local positions = {}
		local positionsCounter = 0
		for s=0, steps do
			local thisStepPosition = startPosition + stepVector * s
			-- Spring.TestBuildOrder
			-- ( number unitDefID, number x, number y, number z, number facing)
			-- -> number blocking [,number featureID]
			-- blocking can be:
			-- 0 - blocked
			-- 1 - mobile unit in the way
			-- 2 - free  (or if featureID is != nil then with a blocking feature that can be reclaimed)
			local blocking,_ = SpringTestBuildOrder(buildingDefID, thisStepPosition.x, thisStepPosition.y, thisStepPosition.z, 0)
			if (blocking > 0) then
				positions[positionsCounter] = thisStepPosition
				positionsCounter = positionsCounter + 1
			end
		end
		
		for p=1, #positions do
			local position = positions[p]
			SpringGiveOrderToUnit(pointUnit, -buildingDefID, position:AsSpringVector(), {"shift"}) -- add into queue
		end
		
		self.inProgress = true -- we are at progress at least 1 frame
		return RUNNING
	else		
		-- wait until buildqueue is empty
		if (self:UnitIdle(pointUnit)) then
			self.inProgress = nil
			return SUCCESS
		end
			
		return RUNNING
	end	
end

function Reset(self)
	self.inProgress = nil
end
