build 0.3
====

* [dependencies](./dependencies.json)

Universal build actions 
---

* lineOfBuildings

Universal build trees
---

* constructBySpotsMask
* constructBySpotsMaskByCategory
* constructBySpotsMaskByCategoryRepeated
* constructInLineByCategory
* constructMultipleLines

Universal build sensors
---

* AvailableUnitByCategoryRandom
