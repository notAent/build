local sensorInfo = {
	name = "AvailableUnitByCategoryRandom",
	desc = "Return unit name based on build options of builder and passed category.",
	author = "PepeAmpere",
	date = "2017-05-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
SpringGetUnitCmdDescs = Spring.GetUnitCmdDescs

-- @description return energy resource info
-- @argument category [table] BETS category 
-- @argument builderUnitID [number|optional] unitID identifying one unit in simulation
return function(category, builderUnitID)
	if (builderUnitID == nil) then builderUnitID = units[1] end -- if there is no unit passed, we expect it is the first unit in list

	local cmdDescs = SpringGetUnitCmdDescs(builderUnitID)
	local listOfUnits = {}
	
	for i=1, #cmdDescs do
		local thisCmdDesc = cmdDescs[i]
		if (category[thisCmdDesc.name] ~= nil) then
			listOfUnits[#listOfUnits + 1] = thisCmdDesc.name
		end
	end
	return listOfUnits[math.random(#listOfUnits)]
end